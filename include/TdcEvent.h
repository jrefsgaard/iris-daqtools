#ifndef TDC_EVENT_H
#define TDC_EVENT_H
#include <queue>
#include <stdint.h> //uint32_t
#include "ModuleEvent.h"

class TdcEvent : public ModuleEvent {
  private:
    
  public:
    TdcEvent() {};
    ~TdcEvent() = default;
    
    //Try to push. fails if event is already complete.
    virtual int TryPush(const uint32_t &word);
};
#endif //TDC_EVENT_H
