#ifndef MODULE_EVENT_H
#define MODULE_EVENT_H
#include <queue>
#include <stdint.h> //uint32_t
#include <mutex>
#include <atomic>

#define EVT_SUCCESS     1
#define EVT_FULL        2
#define EVT_NOTBOE_ERR  3
#define EVT_NOTEOE_ERR  4
#define EVT_NOTDATA_ERR 5
#define EVT_UNKNOWN_ERR 6

class ModuleEvent {
  protected:
    std::mutex buffer_mutex;       //Mutex used to protect the data buffer.
    std::queue<uint32_t> data;     //Data buffer.
    std::atomic<uint64_t> eventId; //Event ID, eg. a time stamp.
    std::atomic<bool> complete;    //Flag signalling if the buffer holds a complete event.
    
  public:
    ModuleEvent() {complete = false; eventId = 0;};
    virtual ~ModuleEvent() = default;
    
    //Try to push. fails if event is already complete.
    virtual int TryPush(const uint32_t &word) = 0;
    
    bool Complete();// {return complete;};
    
    uint64_t EventId();// {return eventId;};
    
    uint32_t Front();
    
    void Pop();
    
    bool Empty();
    
    void Clear();

    void UnsafePop();

    unsigned int Size();
};
#endif //MODULE_EVENT_H
