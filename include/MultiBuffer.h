#ifndef MULTI_BUFFER_H
#define MULTI_BUFFER_H
#include <vector>
#include <stdint.h> //uint32_t
#include <atomic>
#include "SafeQueue.h"

class MultiBuffer {
  private:
    std::atomic<unsigned int> totalSize;  //Size of content.
    std::vector<SafeQueue <uint32_t> > buffers;
    
  public:
    MultiBuffer(int size) {buffers.resize(size); totalSize = 0;};
    ~MultiBuffer() = default;
    
    void push(int i, const uint32_t &word);
    
    void pop(int i) {buffers.at(i).pop(); totalSize--;};
    
    uint32_t front(int i) {return buffers.at(i).front();};
    
    uint32_t back(int i) {return buffers.at(i).back();};
    
    bool empty(int i) { return buffers.at(i).empty();};
    
    size_t size(int i) {return buffers.at(i).size();};
    
    bool empty();
    
    unsigned int size();

    void clear();
};
#endif //MULTI_BUFFER_H
