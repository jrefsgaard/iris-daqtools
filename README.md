# DaqTools

This project provides tools that are used by the Iris DAQ frontend programs. The
classes are mainly related to event reconstruction and data validation for data
from Mesytec MADC-32 ADC modules and CAEN V1190A/B TDC modules.
