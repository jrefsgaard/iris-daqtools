#include "TdcEvent.h"
#include <cstdio>
#include <cstdlib>

using namespace std;

int TdcEvent::TryPush(const uint32_t &word)
{
  //Check if event is already complete.
  if(complete){
    //printf("TdcEvent::TryPush(): Error! Event already complete.\n");
    //return false;
    return EVT_FULL;
  }
  
  lock_guard<mutex> lock(buffer_mutex);  
  //First word must be a BOE word.
  if(data.empty()){
    if((word & 0xf8000000) == 0x40000000){ //Global header word
      data.push(word);
      //return true;
      return EVT_SUCCESS;
    }
    printf("TdcEvent::TryPush(): Error! 0x%08x is not a BOE word.\n",word);
    //exit(EXIT_FAILURE);
    //return false;
    return EVT_NOTBOE_ERR;
  }
  
  //We look for data words.
  if((word & 0xc0000000) == 0x00000000){ //Data word
    data.push(word);
    //return true;
    return EVT_SUCCESS;
  }

  //Global trigger time tag
  if((word & 0xf8000000) == 0x88000000){
    eventId = word & 0x07ffffff;
    data.push(word);
    //return true;
    return EVT_SUCCESS;
  }

  //Filler.
  if((word & 0xf8000000) == 0xc0000000){ 
    data.push(word);
    //return true;
    return EVT_SUCCESS;
  }

  //Global trailer.
  if((word & 0xf8000000) == 0x80000000){
    data.push(word);
    //Check for error.
    if((word & 0x07000000) != 0x00000000){
      printf("TdcEvent::TryPush(): Error status: 0x%08x.\n",word);
      //exit(EXIT_FAILURE);
      return EVT_UNKNOWN_ERR;
    }
    complete = true;
    //return true;
    return EVT_SUCCESS;
  }  

  printf("TdcEvent::TryPush(): Error! 0x%08x is not recognised.\n",word);
  //exit(EXIT_FAILURE);
  //return false;
  return EVT_UNKNOWN_ERR;
}
