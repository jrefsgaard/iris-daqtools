#include "AdcEvent.h"
#include <cstdio>

using namespace std;

int AdcEvent::TryPush(const uint32_t &word)
{
  //Check if event is already complete.
  if(complete){
    //printf("AdcEvent::TryPush(): Error! Event already complete.\n");
    //printf("Returning %d.\n",EVT_FULL);
    //return false;
    return EVT_FULL;
  }

  lock_guard<mutex> lock(buffer_mutex);  
  //First word must be a BOE word.
  if(data.empty()){
    //printf("data.empty()\n");
    if((word & 0xc0000000) == 0x40000000){ //BOE word
      expectedWords = word & 0x000003ff;
      data.push(word);
      return EVT_SUCCESS;
    }
    printf("AdcEvent::TryPush(): Error! 0x%08x is not a BOE word.\n",word);
    //exit(EXIT_FAILURE);
    //return false;
    return EVT_NOTBOE_ERR;
  }
  
  //We look for data words.
  if(expectedWords > 1){
    //int local_expectedWords = expectedWords;
    //printf("exp. words = %d\n",local_expectedWords);
    if((word & 0xff800000) == 0x04000000){ //Data word
      expectedWords--;
      data.push(word);
      //return true;
      return EVT_SUCCESS;
    }
    else if((word & 0xff800000) == 0x04800000){ //Extended time stamp.
      expectedWords--;
      eventId = (uint64_t)(word & 0x0000ffff) << 30;
      data.push(word);
      //return true;
      return EVT_SUCCESS;
    }
    else if(word == 0x00000000){ //Filler (used in 64-bit transfer).
      expectedWords--;
      //data.push(word);
      //return true;
      return EVT_SUCCESS;
    }
    printf("AdcEvent::TryPush(): Error! 0x%08x is not a data word.\n",word);
    //exit(EXIT_FAILURE);
    //return false;
    return EVT_NOTDATA_ERR;
  }
      
  //Check for EOE word.
  if(expectedWords == 1){
    //printf("exp. words == 1\n");
    if((word & 0xc0000000) == 0xc0000000){ //EOE word
      eventId += (word & 0x3fffffff);
      expectedWords--;
      data.push(word);
      complete = true;
      //return true;
      return EVT_SUCCESS;
    }
    printf("AdcEvent::TryPush(): Error! 0x%08x is not an EOE word.\n",word);
    //exit(EXIT_FAILURE);
    //return false;
    return EVT_NOTEOE_ERR;
  }
  
  printf("AdcEvent::TryPush(): Error! 0x%08x is not recognised.\n",word);
  //exit(EXIT_FAILURE);
  //return false;
  return EVT_UNKNOWN_ERR;
}
